import store from './store';
import { bugAdded , bugResolved} from "./actions";
const unsubscribe = store.subscribe(() => {
    console.log("Store Changed !", store.getState());
})
store.dispatch(bugAdded("Bug 1"));
store.dispatch(bugResolved(1));
// store.dispatch({
//     type: actions.BUG_REMOVED,
//     payload:{
//         id:1
//     }
// });
console.log(store.getState())
// import { compose, pipe } from "lodash/fp";

// let input = "   Javascript     ";
// let output = "<div>"+ input.trim() + "</div>";

// const trim = str => str.trim();
// const wrap = type => str => `<${type}>${str}</${type}>`;
// const toLowerCase = str => str.toLowerCase();

// const transform = pipe(trim, toLowerCase, wrap("div"));
// transform(input);
// const result = wrapInDiv(toLowerCase(trim(input))); 